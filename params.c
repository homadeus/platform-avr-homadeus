/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define DEBUG 1
#include "homadeus.h"

//#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>


#include <stdio.h>
#include <string.h>

#include "contiki.h"
#include "contiki-net.h"
#include "cfs/cfs.h"
#include "lib/crc16.h"
#include "dev/watchdog.h"
#include "homadeus.h"
#include "params.h"


#if WITH_NODE_ID
uint16_t node_id;
#endif

#if AVR_WEBSERVER
/* Webserver builds can set these in httpd-fsdata.c via makefsdata.h */
extern uint8_t default_mac_address[8];
extern uint8_t default_server_name[16];
extern uint8_t default_domain_name[30];
#else
const uint8_t default_mac_address[8] PROGMEM = { DEFAULT_EUI64ADDR };
const uint8_t default_server_name[] PROGMEM = PARAMS_SERVERNAME;
const uint8_t default_domain_name[] PROGMEM = PARAMS_DOMAINNAME;
#endif

#define SETTINGS_FILE_NAME_BUFFER_SIZE 10

void init_file_system() {
    watchdog_stop();
    static uint8_t init = 0;
    uint8_t verify[] = HOMADEUS_MAGIC;
    if (!init) {
        eeprom_read_block(verify, 0, HOMADEUS_MAGIC_SIZE);
        if (strncmp_P(verify, PSTR(HOMADEUS_MAGIC), HOMADEUS_MAGIC_SIZE) != 0) {
            cfs_coffee_format();
            strncpy_P(verify, PSTR(HOMADEUS_MAGIC), HOMADEUS_MAGIC_SIZE);
            eeprom_write_block(verify, 0, HOMADEUS_MAGIC_SIZE);
        }
        init = 1;
    }
    watchdog_start();
}

static void settings_write_setting(void* value, const char* id, size_t len) {
    char fname[SETTINGS_FILE_NAME_BUFFER_SIZE];
    uint16_t crc;
    strcpy_P(fname, id);

    watchdog_stop();

    cfs_remove(fname);
    int fd = cfs_open(fname, CFS_WRITE);
    if (fd >= 0) {
        crc = crc16_data(value, len, 0);
        int count = cfs_write(fd, value, len);
        count += cfs_write(fd, &crc, sizeof(crc));
        cfs_close(fd);
    }

    watchdog_start();
}

static void settings_read_setting(void* value, const char* id, size_t len, void* default_value) {
    char fname[SETTINGS_FILE_NAME_BUFFER_SIZE];
    uint16_t crc = 0, crc_read = 0;
    strcpy_P(fname, id);

    int fd = cfs_open(fname, CFS_READ);
    if (fd >= 0) {
        int count = cfs_read(fd, value, len);
        count += cfs_read(fd, &crc_read, sizeof(crc_read));
        crc = crc16_data(value, len, 0);
        cfs_close(fd);
        if(crc != crc_read) {
            goto err;
        }
        return;
    }
err:
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
    PRINTF("[*] Invalid checksum for: %s: %u/%u\n", fname, crc, crc_read);
    memcpy(value, default_value, len);
    settings_write_setting(value, id, len);
    return;
}

#define X_TYPE(id, path, type, typeptr, def)             \
    const char id##_str[] PROGMEM = "s/" #path;          \
	STATIC_ASSERT(sizeof(id##_str) <= SETTINGS_FILE_NAME_BUFFER_SIZE, id##_buffer_is_long_enough); //STATIC_ASSERT(sizeof(id##_str) <= SETTINGS_FILE_NAME_BUFFER_SIZE, buffer_is_long_enough);
        HOMADEUS_SETTINGS
#undef X_TYPE

#define X_TYPE(id, path, type, typeptr, default_value)   \
    void settings_get_##path(typeptr value) {            \
        PRINTF("[-] Get " #path "\n");                       \
        settings_read_setting(                           \
                value,                                   \
                id##_str,                                \
                sizeof(type),                            \
                &(type){default_value}                   \
        );                                               \
    }                                                    \

        HOMADEUS_SETTINGS
#undef X_TYPE
