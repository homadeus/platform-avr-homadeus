/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HOMADEUS_TEMPERATURE_SENSORS_H
#define HOMADEUS_TEMPERATURE_SENSORS_H

#include "sensors/homadeus-sensors.h"

#if defined(HAVE_INTERNAL_TEMP_SENSOR)
extern const struct sensors_sensor internal_temp_sensor;
#endif

#endif /* HOMADEUS_TEMPERATURE_SENSORS_H */
