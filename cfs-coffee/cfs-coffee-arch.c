/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/eeprom.h>

#include "cfs-coffee-arch.h"
#include "dev/eeprom.h"

/* Initialized to '\0' as it's static */
static const unsigned char nullb[COFFEE_SECTOR_SIZE];

/* Erase an eeprom block */
void eeprom_erase(uint16_t sector)
{
    eeprom_write(COFFEE_START + sector * COFFEE_SECTOR_SIZE,
        (unsigned char *)nullb, sizeof(nullb));
}
