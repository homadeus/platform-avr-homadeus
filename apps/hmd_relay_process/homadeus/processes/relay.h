/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOMADEUS_PROCESSES_RELAY_H
#define HOMADEUS_PROCESSES_RELAY_H

#include <avr/io.h>

#include <process.h>
#include <etimer.h>

#define HOMADEUS_RELAY_PROCESS_DESCRIPTION "Homadeus relay process"
#define HOMADEUS_RELAY_PROCESS hmd_relay_process
#define HOMADEUS_RELAY_EVENT hmd_relay_event

extern process_event_t HOMADEUS_RELAY_EVENT;

/* Definition of the relay process */
PROCESS_NAME(HOMADEUS_RELAY_PROCESS);

#define HOMADEUS_RELAY_PORT_DIR DDRD  /* Port D Data Direction Register */
#define HOMADEUS_RELAY_PORT     PORTD /* Port D Data Register */
#define HOMADEUS_RELAY_LED      PD5
#define HOMADEUS_RELAY_SET      PD6
#define HOMADEUS_RELAY_RST      PD7

extern uint8_t homadeus_relay_process_status;

#endif /* end of include guard: HOMADEUS_PROCESSES_RELAY_H */
