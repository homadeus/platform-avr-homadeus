/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Sensor driver for reading the built-in temperature sensor in the CPU.
 */

#include "sensors/temperature-sensors.h"

#ifdef HAVE_INTERNAL_TEMP_SENSOR

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif

#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

// Internal temperature in 0.01C, e.g. 25C is 2500
static int internal_temp_sensor_value(int type) {
  int reading = 0;

  uint8_t oldADCSRA = ADCSRA;
  uint8_t oldADCSRB = ADCSRB;
  uint8_t oldADMUX = ADMUX;

  ADCSRB |= _BV(MUX5);
  ADMUX   = _BV(REFS1) | _BV(REFS0) | 0b1001 ;
  ADCSRA  = _BV(ADEN) | _BV(ADPS0) | _BV(ADPS2) ;

  sbi(ADCSRA, ADSC);
  loop_until_bit_is_clear(ADCSRA,ADSC);

  reading = ADC;

  ADCSRA  = oldADCSRA;  // Restore ADC
  ADCSRB  = oldADCSRB;  // Restore ADC
  ADMUX   = oldADMUX;   // Restore internal vref

  return reading * 113 - 27280;
}

// No configuration needed
static int internal_temp_sensor_configure(int type, int c) {
  return type == SENSORS_ACTIVE;
}

// Temperature sensor is always ready
static int internal_temp_sensor_status(int type) {
  return 1;
}

// Declare temp sensor
SENSORS_SENSOR(
    internal_temp_sensor,
    TEMPERATURE_SENSOR,
    internal_temp_sensor_value,
    internal_temp_sensor_configure,
    internal_temp_sensor_status
);

// vim:cin:ai:sts=2 sw=2
#endif

