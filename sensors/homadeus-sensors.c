/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sensors/homadeus-sensors.h"
#include "sys/energest.h"

#if defined(HAVE_SENSORS)

const struct sensors_sensor *sensors[] = {
#if defined(HAVE_INTERNAL_TEMP_SENSOR)
    &internal_temp_sensor,
#endif
    0
};

unsigned char sensors_flags[(sizeof(sensors) / sizeof(struct sensors_sensor *))];

void homadeus_sensors_activate() {
  struct sensors_sensor *sensor;
  sensor = sensors_first();
  while (sensor) {
    sensor->configure(SENSORS_ACTIVE, 1);
    sensor = sensors_next(sensor);
  }
  ENERGEST_ON(ENERGEST_TYPE_SENSORS);
}

void homadeus_sensors_deactivate() {
  struct sensors_sensor *sensor;
  sensor = sensors_first();
  while (sensor) {
    sensor->configure(SENSORS_ACTIVE, 0);
    sensor = sensors_next(sensor);
  }
  ENERGEST_OFF(ENERGEST_TYPE_SENSORS);
}

#endif /* defined(HAVE_SENSORS) */

