/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOMADEUS_PROCESSES_BOOT_H
#define HOMADEUS_PROCESSES_BOOT_H

#include <avr/io.h>

#include <process.h>
#include <etimer.h>

#define HOMADEUS_BOOT_PROCESS_DESCRIPTION "Homadeus boot process"
#define HOMADEUS_BOOT_PROCESS hmd_boot_process
#define HOMADEUS_BOOT_EVENT hmd_boot_event

/* Definition of the boot process */
PROCESS_NAME(HOMADEUS_BOOT_PROCESS);

#endif /* end of include guard: HOMADEUS_PROCESSES_BOOT_H */
