/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOMADEUS_H
#define HOMADEUS_H

#include <avr/pgmspace.h>
#include <stdio.h>

#include "contiki-net.h"

#define STATIC_ASSERT(COND,MSG) typedef char static_assertion_##MSG[(!!(COND))*2-1]
#define COMPILE_TIME_ASSERT3(X,L) STATIC_ASSERT(X,static_assertion_at_line_##L)
#define COMPILE_TIME_ASSERT2(X,L) COMPILE_TIME_ASSERT3(X,L)
#define COMPILE_TIME_ASSERT(X)    COMPILE_TIME_ASSERT2(X,__LINE__)

#define hmd_printf(FORMAT, args...) printf_P(PSTR(FORMAT), ##args)

#if DEBUG
  #define hmd_debug(FORMAT, args...) \
    hmd_printf(PSTR(FORMAT), ##args)
#else
  #define hmd_debug(...)
#endif


#ifdef UIP_CONF_IPV6
void hmd_ipaddr_print(const uip_ipaddr_t *);
#endif


#endif /* end of include guard: HOMADEUS_H */
