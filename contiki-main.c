/*
 * Copyright (c) 2006, Technical University of Munich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#define ANNOUNCE_BOOT 1    //adds about 600 bytes to program size
#if ANNOUNCE_BOOT
#define PRINTA(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTA(...)
#endif

#define DEBUG 0
#include "homadeus.h"

/* Track interrupt flow through mac, rdc and radio driver */
#if DEBUGFLOWSIZE
uint8_t debugflowsize,debugflow[DEBUGFLOWSIZE];
#define DEBUGFLOW(c) if (debugflowsize<(DEBUGFLOWSIZE-1)) debugflow[debugflowsize++]=c
#else
#define DEBUGFLOW(c)
#endif

#include <avr/pgmspace.h>
#include <avr/fuse.h>
#include <avr/eeprom.h>
#include <stdio.h>
#include <string.h>
#include <dev/watchdog.h>

#include "loader/symbols-def.h"
#include "loader/symtab.h"

#include "radio/rf230bb/rf230bb.h"
#include "net/mac/frame802154.h"
#include "net/mac/framer-802154.h"
#include "net/sicslowpan.h"
#include "params.h"

#include "contiki.h"
#include "contiki-net.h"
#include "contiki-lib.h"

#include "dev/rs232.h"
#include "dev/serial-line.h"
#include "dev/slip.h"

#if AVR_WEBSERVER
#include "httpd-fs.h"
#include "httpd-cgi.h"
#endif

#ifdef COFFEE_FILES
#include "cfs/cfs.h"
#include "cfs/cfs-coffee.h"
#endif

#if UIP_CONF_ROUTER && 0
#include "net/routing/rimeroute.h"
#include "net/rime/rime-udp.h"
#endif

#include "net/rime.h"

#include <avr/signature.h>

#if defined(HAVE_SENSORS)
#include "lib/sensors.h"
#include "sensors/homadeus-sensors.h"
#endif

#include <homadeus/bootloader/bootloader.h>
#include <homadeus/processes/boot.h>
#include <homadeus/boards/board.h>

#if ENERGEST_CONF_ON
#include "lib/print-stats.h"
#endif

/* Get periodic prints from idle loop, from clock seconds or rtimer interrupts */
/* Use of rtimer will conflict with other rtimer interrupts such as contikimac radio cycling */
/* STAMPS will print ENERGEST outputs if that is enabled. */
#define PERIODICPRINTS 1
#if PERIODICPRINTS
//#define PINGS 64
#define ROUTES 600
#define STAMPS 60
#define STACKMONITOR 1024
uint32_t clocktime;
#define TESTRTIMER 0
#if TESTRTIMER
uint8_t rtimerflag=1;
struct rtimer rt;
void rtimercycle(void) {rtimerflag=1;}
#endif
#endif

uint16_t ledtimer;

/*-------------------------------------------------------------------------*/
/*----------------------Configuration of the .elf file---------------------*/
/* JTAG, SPI enabled, Internal RC osc, Boot flash size 4K, 6CK+65msec delay, brownout disabled */
FUSES ={.low = 0xe2, .high = 0x99, .extended = 0xff,};

uint8_t
rng_get_uint8(void) {
#if 1
  /* Upper two RSSI reg bits (RND_VALUE) are random in rf231 */
  uint8_t j;
  j = (PHY_RSSI&0xc0) + ((PHY_RSSI>>2)&0x30) + ((PHY_RSSI>>4)&0x0c) + ((PHY_RSSI>>6)&0x03);
#else
/* Get a pseudo random number using the ADC */
  uint8_t i,j;
  ADCSRA=1<<ADEN;             //Enable ADC, not free running, interrupt disabled, fastest clock
  for (i=0;i<4;i++) {
    ADMUX = 0;                //toggle reference to increase noise
    ADMUX =0x1E;              //Select AREF as reference, measure 1.1 volt bandgap reference.
    ADCSRA|=1<<ADSC;          //Start conversion
    while (ADCSRA&(1<<ADSC)); //Wait till done
	j = (j<<2) + ADC;
  }
  ADCSRA=0;                   //Disable ADC
#endif
  hmd_debug("rng issues %d\n",j);
  return j;
}


static struct spi_slave slave_ = { 0 };
static struct spi_flash flash_ = { 0 };
struct spi_flash *spi_flash = NULL;

static int homadeus_open_flash() {
    slave_.cs_port_dir = &DDRB;
    slave_.cs_port     = &PORTB;
    slave_.cs_pin      = PB0;
    if (spi_flash_probe(&flash_, &slave_)) {
      spi_flash = NULL;
      return 1;
    } else {
      spi_flash = &flash_;
    }

    return 0;
}


/*-------------------------Low level initialization------------------------*/
/*------Done in a subroutine to keep main routine stack usage small--------*/
void initialize(void)
{
  watchdog_init();
  watchdog_start();

#if defined(WITH_SLIP)
  /* Slip border router on second port */
  rs232_init(RS232_PORT_1, USART_BAUD_1000000, USART_PARITY_NONE | USART_STOP_BITS_1 | USART_DATA_BITS_8);
  rs232_set_input(RS232_PORT_1, slip_input_byte);
#endif
  /* First rs232 port for debugging */
  rs232_init(RS232_PORT_0, USART_BAUD_57600, USART_PARITY_NONE | USART_STOP_BITS_1 | USART_DATA_BITS_8);
  /* Redirect stdout to first port */
  rs232_redirect_stdout(RS232_PORT_0);
  /* Get input from first port */
  rs232_set_input(RS232_PORT_0, serial_line_input_byte);

  clock_init();

  if(MCUSR & (1<<PORF )) hmd_debug("Power-on reset.\n");
  if(MCUSR & (1<<EXTRF)) hmd_debug("External reset!\n");
  if(MCUSR & (1<<BORF )) hmd_debug("Brownout reset!\n");
  if(MCUSR & (1<<WDRF )) hmd_debug("Watchdog reset!\n");
  if(MCUSR & (1<<JTRF )) hmd_debug("JTAG reset!\n");

#if STACKMONITOR
  /* Simple stack pointer highwater monitor. Checks for magic numbers in the main
   * loop. In conjuction with PERIODICPRINTS, never-used stack will be printed
   * every STACKMONITOR seconds.
   */
{
extern uint16_t __bss_end;
uint16_t p=(uint16_t)&__bss_end;
    do {
      *(uint16_t *)p = 0x4242;
      p+=10;
    } while (p<SP-10); //don't overwrite our own stack
}
#endif

#define CONF_CALIBRATE_OSCCAL 0
#if CONF_CALIBRATE_OSCCAL
void calibrate_rc_osc_32k();
{
extern uint8_t osccal_calibrated;
uint8_t i;
  hmd_debug("\nBefore calibration OSCCAL=%x\n",OSCCAL);
  for (i=0;i<10;i++) {
    calibrate_rc_osc_32k();
    hmd_debug("Calibrated=%x\n",osccal_calibrated);
//#include <util/delay_basic.h>
//#define delay_us( us )   ( _delay_loop_2(1+(us*F_CPU)/4000000UL) )
//   delay_us(50000);
 }
   clock_init();
}
#endif

  PRINTA("\n*******Booting %s*******\n",CONTIKI_VERSION_STRING);

  PRINTA("Enabling antenna diversity\n");

  ANT_DIV_struct.ant_div_en = 1;
  ANT_DIV_struct.ant_ext_sw_en = 1;

/* rtimers needed for radio cycling */
  rtimer_init();

 /* Initialize process subsystem */
  process_init();

  /* etimers must be started before ctimer_init */
  process_start(&etimer_process, NULL);
  ctimer_init();

  /* Initilaize serial line for input */
  serial_line_init();

  /* Start radio and radio receive process */
  NETSTACK_RADIO.init();

/* Get a random seed for the 802.15.4 packet sequence number.
 * Some layers will ignore duplicates found in a history (e.g. Contikimac)
 * causing the initial packets to be ignored after a short-cycle restart.
 */
  random_init(rng_get_uint8());

  /* Set addresses BEFORE starting tcpip process */
  rimeaddr_t addr;

#if UIP_CONF_IPV6
  /* Format file system if magic number not present */
  init_file_system();

  if (homadeus_open_flash()) {
    settings_get_eui64(addr.u8);
  } else {
    uint8_t serial[8];
    if (spi_flash_read_serial(spi_flash, serial)) {
      settings_get_eui64(addr.u8);
    } else {
      PRINTA("Serial %x-%x-%x-%x-%x-%x-%x-%x\n",serial[0],serial[1],serial[2],serial[3],serial[4],serial[5],serial[6],serial[7]);
      memcpy(&addr.u8, "FOO", 3);
      memcpy(&addr.u8[0] + 3, &serial[0] + 3, sizeof(rimeaddr_t) - 3);
    }
  }

  memcpy(&uip_lladdr.addr, &addr.u8, sizeof(rimeaddr_t));
#elif WITH_NODE_ID
  node_id=get_panaddr_from_eeprom();
  addr.u8[1]=node_id&0xff;
  addr.u8[0]=(node_id&0xff00)>>8;
  PRINTA("Node ID from eeprom: %X\n",node_id);
#endif
  rimeaddr_set_node_addr(&addr);

  uint16_t panid, panaddr;
  uint8_t channel, txpower;
  settings_get_panid(&panid);
  settings_get_panaddr(&panaddr);
  settings_get_channel(&channel);
  settings_get_txpower(&txpower);

  rf230_set_pan_addr(panid, panaddr,(uint8_t *)&addr.u8);
  rf230_set_channel(channel);
  rf230_set_txpower(txpower);

#if UIP_CONF_IPV6
  PRINTA("EUI-64 MAC: %x-%x-%x-%x-%x-%x-%x-%x\n",addr.u8[0],addr.u8[1],addr.u8[2],addr.u8[3],addr.u8[4],addr.u8[5],addr.u8[6],addr.u8[7]);
#else
  PRINTA("MAC address ");
  uint8_t i;
  for (i=sizeof(rimeaddr_t); i>0; i--){
    PRINTA("%x:",addr.u8[i-1]);
  }
  PRINTA("\n");
#endif

  /* Initialize stack protocols */
  queuebuf_init();
  NETSTACK_RDC.init();
  NETSTACK_MAC.init();
  NETSTACK_NETWORK.init();

#if ANNOUNCE_BOOT
  PRINTA("%s %s, channel %u , check rate %u Hz tx power %u\n",NETSTACK_MAC.name, NETSTACK_RDC.name, rf230_get_channel(),
    CLOCK_SECOND / (NETSTACK_RDC.channel_check_interval() == 0 ? 1:NETSTACK_RDC.channel_check_interval()),
    rf230_get_txpower());
#if UIP_CONF_IPV6_RPL
  PRINTA("RPL Enabled\n");
#endif
#if UIP_CONF_ROUTER
  PRINTA("Routing Enabled\n");
#endif

#endif /* ANNOUNCE_BOOT */

  process_start(&tcpip_process, NULL);

#if defined(WITH_SLIP)
  process_start(&slip_process, NULL);
#endif

#if defined(HAVE_SENSORS)
  process_start(&sensors_process, NULL);
  // homadeus_sensors_activate();
#endif

  process_start(&HOMADEUS_BOOT_PROCESS, NULL);

  /* Autostart other processes */
  autostart_start(autostart_processes);

  /*---If using coffee file system create initial web content if necessary---*/
#if COFFEE_FILES
  int fa = cfs_open( "/index.html", CFS_READ);
  if (fa<0) {     //Make some default web content
    PRINTA("No index.html file found, creating upload.html!\n");
    PRINTA("Formatting FLASH file system for coffee...");
    cfs_coffee_format();
    PRINTA("Done!\n");
    fa = cfs_open( "/index.html", CFS_WRITE);
    int r = cfs_write(fa, &"It works!", 9);
    if (r<0) PRINTA("Can''t create /index.html!\n");
    cfs_close(fa);
//  fa = cfs_open("upload.html"), CFW_WRITE);
// <html><body><form action="upload.html" enctype="multipart/form-data" method="post"><input name="userfile" type="file" size="50" /><input value="Upload" type="submit" /></form></body></html>
  }
#endif /* COFFEE_FILES */

/* Add addresses for testing */
#if 0
{
  uip_ip6addr_t ipaddr;
  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
//  uip_ds6_prefix_add(&ipaddr,64,0);
}
#endif

/*--------------------------Announce the configuration---------------------*/
#if ANNOUNCE_BOOT
#if AVR_WEBSERVER
{ uint8_t i;
  char buf[80];
  unsigned int size;

  for (i=0;i<UIP_DS6_ADDR_NB;i++) {
	if (uip_ds6_if.addr_list[i].isused) {
	   httpd_cgi_sprint_ip6(uip_ds6_if.addr_list[i].ipaddr,buf);
       PRINTA("IPv6 Address: %s\n",buf);
	}
  }
   cli();
   eeprom_read_block (buf,eemem_server_name, sizeof(eemem_server_name));
   sei();
   buf[sizeof(eemem_server_name)]=0;
   PRINTA("%s",buf);
   cli();
   eeprom_read_block (buf,eemem_domain_name, sizeof(eemem_domain_name));
   sei();
   buf[sizeof(eemem_domain_name)]=0;
   size=httpd_fs_get_size();
#ifndef COFFEE_FILES
   PRINTA(".%s online with fixed %u byte web content\n",buf,size);
#elif COFFEE_FILES==1
   PRINTA(".%s online with static %u byte EEPROM file system\n",buf,size);
#elif COFFEE_FILES==2
   PRINTA(".%s online with dynamic %u KB EEPROM file system\n",buf,size>>10);
#elif COFFEE_FILES==3
   PRINTA(".%s online with static %u byte program memory file system\n",buf,size);
#elif COFFEE_FILES==4
   PRINTA(".%s online with dynamic %u KB program memory file system\n",buf,size>>10);
#endif /* COFFEE_FILES */
}
#else
   PRINTA("Online\n");
#endif
#endif /* ANNOUNCE_BOOT */

#if RF230BB_CONF_LEDONPORTE1
  /* NB: PORTE1 conflicts with UART0 */
  DDRE|=(1<<DDE1);  //set led pin to output (Micheal Hatrtman board)
  PORTE&=~(1<<PE1); //and low to turn led off
#endif
}

#if ROUTES && UIP_CONF_IPV6

#endif

/*-------------------------------------------------------------------------*/
/*------------------------- Main Scheduler loop----------------------------*/
/*-------------------------------------------------------------------------*/
int
main(void)
{
  initialize();

  while(1) {
    process_run();
    watchdog_periodic();

#if 0
/* Various entry points for debugging in the AVR Studio simulator.
 * Set as next statement and step into the routine.
 */
    NETSTACK_RADIO.send(packetbuf_hdrptr(), 42);
    process_poll(&rf230_process);
    packetbuf_clear();
    len = rf230_read(packetbuf_dataptr(), PACKETBUF_SIZE);
    packetbuf_set_datalen(42);
    NETSTACK_RDC.input();
#endif

#if 0
/* Clock.c can trigger a periodic PLL calibration in the RF230BB driver.
 * This can show when that happens.
 */
    extern uint8_t rf230_calibrated;
    if (rf230_calibrated) {
      hmd_debug("\nRF230 calibrated!\n");
      rf230_calibrated=0;
    }
#endif

/* Set DEBUGFLOWSIZE in contiki-conf.h to track path through MAC, RDC, and RADIO */
#if DEBUGFLOWSIZE
  if (debugflowsize) {
    debugflow[debugflowsize]=0;
    hmd_printf("%s",debugflow);
    debugflowsize=0;
   }
#endif

#if PERIODICPRINTS
#if TESTRTIMER
/* Timeout can be increased up to 8 seconds maximum.
 * A one second cycle is convenient for triggering the various debug printouts.
 * The triggers are staggered to avoid printing everything at once.
 */
    if (rtimerflag) {
      rtimer_set(&rt, RTIMER_NOW()+ RTIMER_ARCH_SECOND*1UL, 1,(void *) rtimercycle, NULL);
      rtimerflag=0;
#else
  if (clocktime!=clock_seconds()) {
     clocktime=clock_seconds();
#endif

#if STAMPS
if ((clocktime%STAMPS)==0) {
#if ENERGEST_CONF_ON
	print_stats();
#elif RADIOSTATS
extern volatile unsigned long radioontime;
  hmd_printf("%u(%u)s\n",clocktime,radioontime);
#else
  hmd_printf("%us\n",clocktime);
#endif

}
#endif
#if TESTRTIMER
      clocktime+=1;
#endif

#if ROUTES && UIP_CONF_IPV6
if ((clocktime%ROUTES)==2) {
 extern uip_ds6_route_t uip_ds6_routing_table[];

extern uip_ds6_nbr_t uip_ds6_nbr_cache[];
extern uip_ds6_netif_t uip_ds6_if;

  uint8_t i,j;
  hmd_printf("\nAddresses [%u max]\n",UIP_DS6_ADDR_NB);
  for (i=0;i<UIP_DS6_ADDR_NB;i++) {
    if (uip_ds6_if.addr_list[i].isused) {
      hmd_ipaddr_print(&uip_ds6_if.addr_list[i].ipaddr);
      hmd_printf("\n");
    }
  }
  hmd_printf("\nNeighbors [%u max]\n",UIP_DS6_NBR_NB);
  for(i = 0,j=1; i < UIP_DS6_NBR_NB; i++) {
    if(uip_ds6_nbr_cache[i].isused) {
      hmd_ipaddr_print(&uip_ds6_nbr_cache[i].ipaddr);
      hmd_printf("\n");
      j=0;
    }
  }
  if (j) hmd_printf("  <none>");
  hmd_printf("\nRoutes [%u max]\n",UIP_DS6_ROUTE_NB);
  {
    uip_ds6_route_t *r;
    hmd_printf("\nRoutes [%u max]\n",UIP_DS6_ROUTE_NB);
    j = 1;
    for(r = uip_ds6_route_list_head();
        r != NULL;
        r = list_item_next(r)) {
      hmd_ipaddr_print(&r->ipaddr);
      hmd_printf("/%u (via ", r->length);
      hmd_ipaddr_print(&r->nexthop);
      hmd_printf(") %lus\n", r->state.lifetime);
      j = 0;
    }
  }
  if (j) hmd_printf("  <none>");
  hmd_printf("\n---------\n");
}
#endif

#if STACKMONITOR
if ((clocktime%STACKMONITOR)==3) {
  extern uint16_t __bss_end;
  uint16_t p=(uint16_t)&__bss_end;
  do {
    if (*(uint16_t *)p != 0x4242) {
      hmd_printf("Never-used stack > %d bytes\n",p-(uint16_t)&__bss_end);
      break;
    }
    p+=10;
  } while (p<RAMEND-10);
}
#endif

    }
#endif /* PERIODICPRINTS */

#if RF230BB&&0
extern uint8_t rf230processflag;
    if (rf230processflag) {
      hmd_printf("rf230p%d",rf230processflag);
      rf230processflag=0;
    }
#endif

#if RF230BB&&0
extern uint8_t rf230_interrupt_flag;
    if (rf230_interrupt_flag) {
 //   if (rf230_interrupt_flag!=11) {
        hmd_printf("**RI%u",rf230_interrupt_flag);
 //   }
      rf230_interrupt_flag=0;
    }
#endif
  }
  return 0;
}

/*---------------------------------------------------------------------------*/

void log_message(char *m1, char *m2)
{
  hmd_printf("%s%s\n", m1, m2);
}
