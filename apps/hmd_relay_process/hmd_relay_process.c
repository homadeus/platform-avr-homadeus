/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pt.h>
#include <stdio.h>

#include <homadeus/processes/relay.h>

#define DEBUG 1
#include <homadeus/utils/debug.h>

/* Process event for updating relay status */
process_event_t HOMADEUS_RELAY_EVENT;

/* Definition of the relay process */
PROCESS(hmd_relay_process, HOMADEUS_RELAY_PROCESS_DESCRIPTION);

/* Implementation of the relay process */
PROCESS_THREAD(hmd_relay_process, ev, data) {
  static struct etimer timer;

  PROCESS_BEGIN();

  HOMADEUS_RELAY_PORT &=		// Set SET SET and LED pins to value 0
    ~_BV(HOMADEUS_RELAY_LED) &
    ~_BV(HOMADEUS_RELAY_SET) &
    ~_BV(HOMADEUS_RELAY_RST);


  HOMADEUS_RELAY_PORT_DIR |=  // Set RST, SET and LED pins to OUTPUT state // tri-state version sets to INPUT state
    _BV(HOMADEUS_RELAY_LED) |
    _BV(HOMADEUS_RELAY_RST) |
    _BV(HOMADEUS_RELAY_SET);


  /***************************
   * Set the Relay OFF
   */

  // For the new relay hardware we need a longer initial waiting time to set the relay off.
  etimer_set(&timer, CLOCK_CONF_SECOND * 2);
  etimer_restart(&timer);

  //HOMADEUS_RELAY_PORT_DIR |= _BV(HOMADEUS_RELAY_RST);	// Set RST pin to OUTPUT
  HOMADEUS_RELAY_PORT |= _BV(HOMADEUS_RELAY_RST);	 	// Set RST pin to value 1 --> Relay OFF

  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&timer));

  HOMADEUS_RELAY_PORT &= ~_BV(HOMADEUS_RELAY_RST);	  	// Set RST pin to value 0
  // tri-state version:
  //HOMADEUS_RELAY_PORT_DIR &= ~_BV(HOMADEUS_RELAY_RST);	// Set RST pins to INPUT state
  //HOMADEUS_RELAY_PORT &= ~_BV(HOMADEUS_RELAY_RST);	  	// Set RST pin to value 0 --> no pull-up resistor activated (floating value)

  // Timer back to CLOCK_CONF_SECOND / 4 period.
  etimer_set(&timer, CLOCK_CONF_SECOND / 4);

  /***************************/

  HOMADEUS_RELAY_EVENT = process_alloc_event();

  while (1) {
    // wait until we get a data_ready event
    PROCESS_WAIT_EVENT_UNTIL(ev == HOMADEUS_RELAY_EVENT);

    if (*(uint8_t*)data) {
    	//Logic for turning ON the RELAY
    	//HOMADEUS_RELAY_PORT_DIR |= _BV(HOMADEUS_RELAY_SET); // Set SET pin to OUTPUT
    	HOMADEUS_RELAY_PORT |= _BV(HOMADEUS_RELAY_SET) | _BV(HOMADEUS_RELAY_LED);		// Set SET and LED pin to value 1 --> Relay ON / LED ON

    } else {
    	//Logic for turning OFF the RELAY
    	//HOMADEUS_RELAY_PORT_DIR |= _BV(HOMADEUS_RELAY_RST);	// Set RST pin to OUTPUT
    	HOMADEUS_RELAY_PORT |= _BV(HOMADEUS_RELAY_RST);		// Set RST pin to value 1 --> Relay FF
    	HOMADEUS_RELAY_PORT &= ~_BV(HOMADEUS_RELAY_LED);	// Set LED pin to value 0 --> LED OFF
    }

    etimer_restart(&timer);
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&timer));

    HOMADEUS_RELAY_PORT &=
          ~_BV(HOMADEUS_RELAY_SET) &	// Set SET and RST pins to value 0
          ~_BV(HOMADEUS_RELAY_RST);

    /*// tri-state version:
    HOMADEUS_RELAY_PORT &=
      ~_BV(HOMADEUS_RELAY_SET) &	// Set SET and RST pins to value 0 --> 	IF OUTPUT: set pin to value 0
      ~_BV(HOMADEUS_RELAY_RST);		//										IF INPUT:  no pull-up resistor activated (floating value)

    HOMADEUS_RELAY_PORT_DIR &= 		// Set RST, SET and LED pins to INPUT state
      ~_BV(HOMADEUS_RELAY_LED) &
      ~_BV(HOMADEUS_RELAY_SET) &
      ~_BV(HOMADEUS_RELAY_RST);

    */
  }


  PROCESS_END();
}
