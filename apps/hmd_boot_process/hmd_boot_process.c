/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pt.h>
#include <stdio.h>

#include <homadeus/processes/boot.h>
#include <homadeus/bootloader/bootloader.h>

#define DEBUG 1
#include <homadeus/utils/debug.h>

/* Definition of the boot process */
PROCESS(hmd_boot_process, HOMADEUS_BOOT_PROCESS_DESCRIPTION);

/* Implementation of the boot process */
PROCESS_THREAD(hmd_boot_process, ev, data) {
  static struct etimer et;

  PROCESS_BEGIN();

  etimer_set(&et, CLOCK_SECOND * 15);
  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

  PRINTF("Marking boot as OK\n");
  bootloader_status_ok();

  PROCESS_END();
}

