/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOMADEUS_CFS_COFFEE_ARCH_H
#define HOMADEUS_CFS_COFFEE_ARCH_H

#include "contiki-conf.h"
#include "dev/eeprom.h"

/* Starting address of the coffee file system */
#define COFFEE_ADDRESS            (COFFEE_PAGE_SIZE*4)

/* Byte page size, starting address, and size of the file system */
#define COFFEE_PAGE_SIZE          16UL
#define COFFEE_START              COFFEE_ADDRESS
#define COFFEE_SIZE               ((2 * 1024U) - COFFEE_START)

/* These must agree with the parameters passed to makefsdata */
#define COFFEE_SECTOR_SIZE        (COFFEE_PAGE_SIZE*4)
#define COFFEE_NAME_LENGTH        16

/* These are used internally by the coffee file system */
#define COFFEE_MAX_OPEN_FILES     4
#define COFFEE_FD_SET_SIZE        8
#define COFFEE_LOG_TABLE_LIMIT    16
#define COFFEE_DYN_SIZE           (COFFEE_PAGE_SIZE * 4)
#define COFFEE_LOG_SIZE           128
#define COFFEE_IO_SEMANTICS       1
#define COFFEE_APPEND_ONLY        0
#define COFFEE_MICRO_LOGS         0


#define COFFEE_ERASE(sector) \
        eeprom_erase(sector)

#define COFFEE_WRITE(buf, size, offset) \
        eeprom_write(COFFEE_START + (offset), (unsigned char *)(buf), (size))

#define COFFEE_READ(buf, size, offset) \
        eeprom_read (COFFEE_START + (offset), (unsigned char *)(buf), (size))

typedef uint16_t coffee_offset_t;
typedef int16_t coffee_page_t;

extern void eeprom_erase(uint16_t sector);

#endif /* HOMADEUS_CFS_COFFEE_ARCH_H */
