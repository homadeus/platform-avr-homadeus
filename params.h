/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOMADEUS_PARAMS_H
#define HOMADEUS_PARAMS_H

#include "radio/rf230bb/rf230bb.h"
#include "net/mac/frame802154.h"

#if AVR_WEBSERVER
/* Webserver builds can set some defaults in httpd-fsdata.c via makefsdata.h */
extern uint8_t eemem_mac_address[8];
extern uint8_t eemem_server_name[16];
extern uint8_t eemem_domain_name[30];
#endif

#define HOMADEUS_MAGIC "H0MAD3US"
#define HOMADEUS_MAGIC_SIZE (sizeof(HOMADEUS_MAGIC) - 1)

#ifdef CHANNEL_802_15_4
#define DEFAULT_CHANNEL CHANNEL_802_15_4
#else
#define DEFAULT_CHANNEL 26
#endif

#ifdef IEEE802154_PANID
#define DEFAULT_PANID IEEE802154_PANID
#else
#define DEFAULT_PANID 0xABCD
#endif

#ifdef IEEE802154_PANADDR
#define DEFAULT_PANADDR IEEE802154_PANADDR
#else
#define DEFAULT_PANADDR 0
#endif

#ifdef RF230_MAX_TX_POWER
#define DEFAULT_TXPOWER RF230_MAX_TX_POWER
#else
#define DEFAULT_TXPOWER 0
#endif

#if UIP_CONF_LL_802154
#define DEFAULT_EUI64ADDR 0x02, 0x00, 0x00, 0xff, 0xfe, 0x00, 0x00, 0x01
#else
#define DEFAULT_EUI64ADDR 0x00, 0x00, 0x00, 0xff, 0xfe, 0x00, 0x00, 0x01
#endif

#define HOMADEUS_SETTINGS \
	X_TYPE  ( CHANNEL,   channel, uint8_t   , uint8_t* , DEFAULT_CHANNEL   ) \
	X_TYPE  ( TXPOWER,   txpower, uint8_t   , uint8_t* , DEFAULT_TXPOWER   ) \
	X_TYPE  ( PANID  ,   panid  , uint16_t  , uint16_t*, DEFAULT_PANID     ) \
	X_TYPE  ( PANADDR,   panaddr, uint16_t  , uint16_t*, DEFAULT_PANADDR   ) \
        X_TYPE  ( EUI64,     eui64  , uint8_t[8], uint8_t* , DEFAULT_EUI64ADDR )


#ifdef SERVER_NAME
#define PARAMS_SERVERNAME SERVER_NAME
#else
#define PARAMS_SERVERNAME "ATMEGA128rfa1"
#endif
#ifdef DOMAIN_NAME
#define PARAMS_DOMAINNAME DOMAIN_NAME
#else
#define PARAMS_DOMAINNAME "localhost"
#endif
#ifdef NODE_ID
#define PARAMS_NODEID NODE_ID
#else
#define PARAMS_NODEID 0
#endif


#define X_TYPE(id, path, type, typeptr, def_value)    \
    extern void settings_get_##path(typeptr);
        HOMADEUS_SETTINGS
#undef X_TYPE

extern void init_file_system();

#endif /* HOMADEUS_PARAMS_H */
