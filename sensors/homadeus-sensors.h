/*
 * Homadeus Platform Driver
 * Copyright (C) 2012 Homadeus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOMADEUS_HOMADEUS_SENSORS_H
#define HOMADEUS_HOMADEUS_SENSORS_H

#include "contiki-conf.h"

#if defined(HAVE_SENSORS)

#include "sensors/temperature-sensors.h"

#define TEMPERATURE_SENSOR "Temperature"

#include "lib/sensors.h"

void homadeus_sensors_activate();
void homadeus_sensors_deactivate();

#endif /* defined(HAVE_SENSORS) */
#endif /* HOMADEUS_HOMADEUS_SENSORS_H */
